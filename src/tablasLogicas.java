
public class tablasLogicas {
	public static char logicoOr(char c, char d) {
		if(c == 'F' && d == 'F')
			return 'F';
		if(c == 'F' && d == 'V')
			return 'V';
		if(c == 'V' && d == 'F')
			return 'V';
		if(c == 'V' && d == 'V')
			return 'V';
		
		return 'F';
	}
	
	public static char logicoAnd(char c, char d) {
		if(c == 'F' && d == 'F')
			return 'F';
		if(c == 'F' && d == 'V')
			return 'F';
		if(c == 'V' && d == 'F')
			return 'F';
		if(c == 'V' && d == 'V')
			return 'V';
		
		return 'F';
	}
	public static char logicoNegar(char c) {
		if(c == 'V')
			return 'F';
		if(c == 'F')
			return 'V';
		
		return 'V';
	}
	
	//SE CAMBIA EL ORDEN DE C Y D PUES EN LA PILA PRIMERO SALE D Y DESPUES C
	public static char logicoEntonces(char c, char d) {
		if(d == 'F' && c== 'F')
			return 'V';
		if(d == 'F' && c == 'V')
			return 'V';
		if(d == 'V' && c == 'F')
			return 'F';
		if(d == 'V' && c == 'V')
			return 'V';
		
		return 'F';
	}
	
	//SE CAMBIA EL ORDEN DE C Y D PUES EN LA PILA PRIMERO SALE D Y DESPUES C
	public static char logicoDobleImplicacion(char c, char d) {
		if(d == 'F' && c == 'F')
			return 'V';
		if(d == 'F' && c == 'V')
			return 'F';
		if(d== 'V' && c == 'F')
			return 'F';
		if(d == 'V' && c == 'V')
			return 'V';
		
		return 'V';
	}
}
