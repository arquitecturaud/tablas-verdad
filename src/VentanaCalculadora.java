import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.GridBagLayout;
import javax.swing.JButton;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class VentanaCalculadora extends JFrame {
	private JLabel txtEntradaUsuario;
	private String textoReal = "";
	/**
	 * Create the frame.
	 */
	public VentanaCalculadora() {
		getContentPane().setLayout(null);
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		//Se agrega el titulo
		JLabel lblCalculadoraProposiciones = new JLabel("CALCULADORA PROPOSICIONES", SwingConstants.CENTER);
		getContentPane().add(lblCalculadoraProposiciones, BorderLayout.NORTH);
		
		//Se agrega el panel que contiene la calculadora
		JPanel contenedorCalculadora = new JPanel();
		getContentPane().add(contenedorCalculadora, BorderLayout.CENTER);
		contenedorCalculadora.setLayout(new BorderLayout(0, 0));
		
		//Se agrega el campo para visualizar lo que se esta haciendo
		txtEntradaUsuario = new JLabel("CON LOS BOTONES INGRESE SU EXPRESION ",SwingConstants.CENTER);
		txtEntradaUsuario.setText(" ");
		txtEntradaUsuario.setBackground(Color.lightGray);
		txtEntradaUsuario.setOpaque(true);
		contenedorCalculadora.add(txtEntradaUsuario, BorderLayout.NORTH);
		
		//Se agregan los caracteres de la calculadora
		JPanel contenedorCaracteres = new JPanel();
		contenedorCalculadora.add(contenedorCaracteres, BorderLayout.CENTER);
		GridBagLayout gbl_contenedorCaracteres = new GridBagLayout();
		gbl_contenedorCaracteres.columnWidths = new int[]{0, 0, 0, 0, 0};
		gbl_contenedorCaracteres.rowHeights = new int[]{0, 0, 0, 0};
		gbl_contenedorCaracteres.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_contenedorCaracteres.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		contenedorCaracteres.setLayout(gbl_contenedorCaracteres);
		
		JButton btnP = new JButton("P");
		GridBagConstraints gbc_btnP = new GridBagConstraints();
		gbc_btnP.insets = new Insets(0, 0, 5, 5);
		gbc_btnP.gridx = 0;
		gbc_btnP.gridy = 0;
		gbc_btnP.weighty = 1.0;
		gbc_btnP.weightx = 1.0;
		gbc_btnP.fill = GridBagConstraints.BOTH;
		contenedorCaracteres.add(btnP, gbc_btnP);
		
		JButton btnQ = new JButton("Q");
		GridBagConstraints gbc_btnQ = new GridBagConstraints();
		gbc_btnQ.insets = new Insets(0, 0, 5, 5);
		gbc_btnQ.gridx = 1;
		gbc_btnQ.gridy = 0;
		gbc_btnQ.weighty = 1.0;
		gbc_btnQ.weightx = 1.0;
		gbc_btnQ.fill = GridBagConstraints.BOTH;

		contenedorCaracteres.add(btnQ, gbc_btnQ);
		
		JButton btnParentesisAbierto = new JButton("(");
		GridBagConstraints gbc_btnParentesisAbierto = new GridBagConstraints();
		gbc_btnParentesisAbierto.insets = new Insets(0, 0, 5, 5);
		gbc_btnParentesisAbierto.gridx = 2;
		gbc_btnParentesisAbierto.gridy = 0;
		gbc_btnParentesisAbierto.weighty = 1.0;
		gbc_btnParentesisAbierto.weightx = 1.0;
		gbc_btnParentesisAbierto.fill = GridBagConstraints.BOTH;
		contenedorCaracteres.add(btnParentesisAbierto,gbc_btnParentesisAbierto);

		
		JButton btnDobleImp = new JButton("\u21d4");
		GridBagConstraints gbc_btnDobleImp = new GridBagConstraints();
		gbc_btnDobleImp.insets = new Insets(0, 0, 5, 5);
		gbc_btnDobleImp.gridx = 2;
		gbc_btnDobleImp.gridy = 2;
		gbc_btnDobleImp.weighty = 1.0;
		gbc_btnDobleImp.weightx = 1.0;
		gbc_btnDobleImp.fill = GridBagConstraints.BOTH;
		contenedorCaracteres.add(btnDobleImp, gbc_btnDobleImp);
		
		JButton btnCierreParentesis = new JButton(")");
		GridBagConstraints gbc_btnCierreParentesis = new GridBagConstraints();
		gbc_btnCierreParentesis.insets = new Insets(0, 0, 5, 0);
		gbc_btnCierreParentesis.gridx = 3;
		gbc_btnCierreParentesis.gridy = 0;
		gbc_btnCierreParentesis.weighty = 1.0;
		gbc_btnCierreParentesis.weightx = 1.0;
		gbc_btnCierreParentesis.fill = GridBagConstraints.BOTH;

		contenedorCaracteres.add(btnCierreParentesis, gbc_btnCierreParentesis);
		
		JButton btnY = new JButton("\u2227");
		GridBagConstraints gbc_btnY = new GridBagConstraints();
		gbc_btnY.insets = new Insets(0, 0, 5, 5);
		gbc_btnY.gridx = 0;
		gbc_btnY.gridy = 1;
		gbc_btnY.weighty = 1.0;
		gbc_btnY.weightx = 1.0;
		gbc_btnY.fill = GridBagConstraints.BOTH;

		contenedorCaracteres.add(btnY, gbc_btnY);
		
		JButton btnO = new JButton("\u2228");
		GridBagConstraints gbc_btnO = new GridBagConstraints();
		gbc_btnO.insets = new Insets(0, 0, 5, 5);
		gbc_btnO.gridx = 1;
		gbc_btnO.gridy = 1;
		gbc_btnO.weighty = 1.0;
		gbc_btnO.weightx = 1.0;
		gbc_btnO.fill = GridBagConstraints.BOTH;

		contenedorCaracteres.add(btnO, gbc_btnO);
		
		JButton btnEntonces = new JButton("\u2192");
		GridBagConstraints gbc_btnEntonces = new GridBagConstraints();
		gbc_btnEntonces.insets = new Insets(0, 0, 5, 5);
		gbc_btnEntonces.gridx = 2;
		gbc_btnEntonces.gridy = 1;
		gbc_btnEntonces.weighty = 1.0;
		gbc_btnEntonces.weightx = 1.0;
		gbc_btnEntonces.fill = GridBagConstraints.BOTH;

		contenedorCaracteres.add(btnEntonces, gbc_btnEntonces);
		
		JButton btnIgual = new JButton("=");
		GridBagConstraints gbc_btnIgual = new GridBagConstraints();
		gbc_btnIgual.insets = new Insets(0, 0, 5, 0);
		gbc_btnIgual.gridx = 3;
		gbc_btnIgual.gridy = 1;
		gbc_btnIgual.weighty = 1.0;
		gbc_btnIgual.weightx = 1.0;
		gbc_btnIgual.fill = GridBagConstraints.BOTH;

		contenedorCaracteres.add(btnIgual, gbc_btnIgual);
		
		JButton btnBorrar = new JButton("Borrar ultimo");
		GridBagConstraints gbc_btnBorrar = new GridBagConstraints();
		gbc_btnBorrar.insets = new Insets(0, 0, 0, 5);
		gbc_btnBorrar.gridx = 0;
		gbc_btnBorrar.gridy = 2;
		gbc_btnBorrar.weighty = 1.0;
		gbc_btnBorrar.weightx = 1.0;
		gbc_btnBorrar.fill = GridBagConstraints.BOTH;

		contenedorCaracteres.add(btnBorrar, gbc_btnBorrar);
		
		JButton btnNegar = new JButton("~");
		GridBagConstraints gbc_btnNegar = new GridBagConstraints();
		gbc_btnNegar.insets = new Insets(0, 0, 0, 5);
		gbc_btnNegar.gridx = 1;
		gbc_btnNegar.gridy = 2;
		gbc_btnNegar.weighty = 1.0;
		gbc_btnNegar.weightx = 1.0;
		gbc_btnNegar.fill = GridBagConstraints.BOTH;

		contenedorCaracteres.add(btnNegar, gbc_btnNegar);
		
		//El campo del resultado
		JLabel lblResultado = new JLabel("RES");
		getContentPane().add(lblResultado, BorderLayout.SOUTH);
		
		//EVENTOS
		
		/*
		 * O =  logico OR
		 * Y = logico AND
		 * E = logico ENTONCES
		 * B = logico DOBLE IMPLICACION
		 */
		CalculadoraLogico logico = new CalculadoraLogico();
		
		btnP.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent r) {
				txtEntradaUsuario.setText(txtEntradaUsuario.getText() + "P");
				textoReal += "P";
			}
		});
		btnQ.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent r) {
				txtEntradaUsuario.setText(txtEntradaUsuario.getText() + "Q");
				textoReal +=  "Q";
			}
		});
		btnParentesisAbierto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent r) {
				txtEntradaUsuario.setText(txtEntradaUsuario.getText() + "(");
				textoReal +="(";

			}
		});
		btnCierreParentesis.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent r) {
				txtEntradaUsuario.setText(txtEntradaUsuario.getText() + ")");
				textoReal += ")";

			}
		});
		btnY.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent r) {
				txtEntradaUsuario.setText(txtEntradaUsuario.getText() + "\u2227");
				textoReal +=  "Y";

			}
		});
		btnO.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent r) {
				txtEntradaUsuario.setText(txtEntradaUsuario.getText() + "\u2228");
				textoReal += "O";

			}
		});
		btnEntonces.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent r) {
				txtEntradaUsuario.setText(txtEntradaUsuario.getText() + "\u2192");
				textoReal += "E";

			}
		});
		btnDobleImp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent r) {
				txtEntradaUsuario.setText(txtEntradaUsuario.getText() + "\u21d4");
				textoReal += "D";

			}
		});
		btnNegar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent r) {
				txtEntradaUsuario.setText(txtEntradaUsuario.getText() + "~");
				textoReal += "N";

			}
		});
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent r) {
				if(txtEntradaUsuario.getText().length() >1) {
				txtEntradaUsuario.setText(txtEntradaUsuario.getText().substring(0, txtEntradaUsuario.getText().length() - 1));
				textoReal = textoReal.substring(0, textoReal.length() - 1);

				}else {
				txtEntradaUsuario.setText(" ");
				textoReal = "";
				}
			}
		});
		btnIgual.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent r) {
				CalculadoraLogico solver = new CalculadoraLogico();
				lblResultado.setText(solver.solucionar(textoReal));
			}
		});
	}

}
