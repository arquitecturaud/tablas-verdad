import java.util.Arrays;
import java.util.Stack;

import javax.swing.JOptionPane;

public class CalculadoraLogico {
	public String solucionar(String proposicion) {
			//LA SALIDA SE PASARA AL JLABEL EL CUAL INTERPRETA HTML
			String salida = "<html>";
			salida += "RESULTADO" + "<br>";
			//LA PILA CON LA CUAL SE SOLUCIONA LA NOTACION POLACA
			Stack pila = new Stack();
			
			//SE PASA EL STRING DEVUELTO A UN ARREGLO DE CHAR
			char[] polaca = notacionPolaca.aPolaca(proposicion).toCharArray();
			JOptionPane.showMessageDialog(null,  notacionPolaca.aPolaca(proposicion));
			//LAS DOS VARIABLES A TENER EN CUENTA
			char p[] = {'V','V','F','F'};
			char q[] = {'V','F','V','F'};

			
			
			//BOOLEANO PARA LA VALIDACION DE LAS NEGACIONES
			boolean negar = false;
			
			//Se recorre cada combinacion
			for(int i = 0 ; i<4 ; i++) {
				//Se recorre toda la expresion
				for (int j = 0 ; j<polaca.length ; j++) {

					//SI HAY UNA N ANTES
					if (polaca[j] == 'N') {
						negar = true;
					}
					
					//SE ENTRA SOLO SI HUBO UNA N ANTES (MAS NO SI SE EVALUA UNA N)
					if(negar == true && polaca[j] != 'N') {
						if(polaca[j] == 'P') {
							pila.push(tablasLogicas.logicoNegar(p[i]));
						}
						if(polaca[j] == 'Q') {
							pila.push(tablasLogicas.logicoNegar(q[i]));
						}
						negar = false;
						
					//SE ENTRA SOLO SI NO HAY NEGACION
					}else if(negar == false) {
						if(polaca[j] == 'P') {
							pila.push(p[i]);
						}
						if(polaca[j] == 'Q') {
							pila.push(q[i]);
						}
					}
					/*
					 * O =  logico OR
					 * Y = logico AND
					 * E = logico ENTONCES
					 * D = logico DOBLE IMPLICACION
					 */
					//ANALISIS DE LAS OPCIONES LOGICAS
					if(polaca[j] == 'O') {
						pila.push(tablasLogicas.logicoOr((char) pila.pop() ,(char) pila.pop()));
					}
					if(polaca[j] == 'Y') {
						pila.push(tablasLogicas.logicoAnd((char) pila.pop() ,(char) pila.pop()));
					}
					if(polaca[j] == 'E') {
						pila.push(tablasLogicas.logicoEntonces((char) pila.pop() ,(char) pila.pop()));
					}
					if(polaca[j] == 'D') {
						pila.push(tablasLogicas.logicoDobleImplicacion((char) pila.pop() ,(char) pila.pop()));
					}
				}
				//SE VA AGREGANDO CADA RESULTADO A SALIDA
				salida += pila.pop() + "<br>";
			}
			//SE CIERRA EL HTML INTERPRETADO POR EL JLABEL	
		return salida + "</html>";
	}
}
