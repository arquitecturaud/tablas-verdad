import java.util.Stack;

public class notacionPolaca {
	//SE TRANSFORMA LA NOTACION ALGEBRAICA A NOTACION POLACA
		public static String aPolaca (String proposicion) {	
			char c ;
			String finalPolaca = "";
			char[] ch = proposicion.toCharArray() ;      
			/*
			 * O =  logico OR
			 * Y = logico AND
			 * E = logico ENTONCES
			 * B = logico DOBLE IMPLICACION
			 */
	        int contador = 0;
	        Stack pila = new Stack();       
	        for (int j = 0 ; j<ch.length;j++) {
	        	c=ch[j];
	            if (c == 'P' || c == 'Q' || c == 'N') {
	            	finalPolaca += c;
	            }
	            if(c == ')') {
	            	finalPolaca += pila.pop();
	            }if (c != '(' && c!='N') {
	            	finalPolaca+=" ";
	            }
	            if(c == 'Y' || c == 'O' || c == 'E' || c == 'B'  || c == 'D'   ) {
	            	pila.push(c);
	            }  
	        }
			return finalPolaca;
		}
}
